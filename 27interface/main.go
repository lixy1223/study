package main

import (
	"fmt"
	"math/rand"
	"sort"
)

// type AInterface interface {
// 	test01()
// 	test02()
// }

// type BInterface interface {
// 	test01()
// 	test03()
// }

// type CInterface interface {
// 	AInterface
// 	BInterface
// }

//定义结构体
type Stu struct {
	Name  string
	Age   int
	Score float64
}

//转切片
type StuSlice []Stu

//实现Interface接口中的三个方法
func (s StuSlice) Len() int {
	return len(s)
}

func (s StuSlice) Less(i, j int) bool {
	return s[i].Name < s[j].Name
}

func (s StuSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// func (s *Stu) test02() {

// }

// func (s *Stu) test03() {

// }

func main() {

	// var stu Stu
	// var a AInterface = &stu
	// var b BInterface = &stu
	// var c CInterface = &stu
	// fmt.Println("ok~", a, b, c)

	var stus StuSlice
	for i := 0; i < 10; i++ {
		stu := Stu{
			Name:  fmt.Sprintf("学生~%d", rand.Intn(100)),
			Age:   rand.Intn(100),
			Score: float64(rand.Intn(100)),
		}
		stus = append(stus, stu)
	}
	for _, v := range stus {
		fmt.Println(v)
	}
	sort.Sort(stus)
	fmt.Println("------排序后-------")
	for _, v := range stus {
		fmt.Println(v)
	}
}
