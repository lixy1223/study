//打印空心金字塔

package main
import (
	"fmt"
)
func main() {
	var tower int = 7  //总层数
	for i := 1 ; i <= tower ; i++ { //当前层数i
		//空格数k
		for k := 1 ; k <= tower - i ; k++ {
			fmt.Print(" ")
		}
		//*数j
		for j := 1 ; j <= 2 * i - 1 ; j++ {
			//第一个+最后一个+最后一层
			if i == tower || j == 2 * i - 1 || j == 1 { 
			fmt.Print("*")
			} else {
				fmt.Print(" ")
			}
			
		}
			
		fmt.Println() //换行
	}	
}