package main

//家庭收支管理软件
import (
	"fmt"
)

func main() {
	var operation string
	var money float64 = 10000                   //余额
	var change float64                          //收支变化
	var details string = "收支\t收支金额\t账户金额\t说  明" //收支明细
	var note string = ""                        //收支说明
	loop := true
	for {
		fmt.Println("--------------------------家庭收支管理---------------------------")
		fmt.Println("----------------------------------------------------------------")
		fmt.Println("                          1. 收支明细")
		fmt.Println("                          2. 登记收入")
		fmt.Println("                          3. 登记支出")
		fmt.Println("                          4.   退出")
		fmt.Println("----------------------------------------------------------------")
		fmt.Println("请输入想要进行的操作< 1~4 >")
		fmt.Scanln(&operation)
		switch operation {
		case "1":
			if details == "收支\t收支金额\t账户金额\t说  明" {
				fmt.Println("还没有收支记录，快记录下第一笔收支吧！")
			} else {
				fmt.Println(details)
			}
		case "2":
			fmt.Println("请输入收入金额")
			fmt.Scanln(&change)
			money += change
			fmt.Println("请输入收入说明")
			fmt.Scanln(&note)
			details += fmt.Sprintf("\n收入\t%.2f\t\t%.2f\t%s", change, money, note)
		case "3":
			fmt.Println("请输入支出金额")
			fmt.Scanln(&change)
			if change > money {
				fmt.Println("余额不足！")
			} else {
				money -= change
				fmt.Println("请输入支出说明")
				fmt.Scanln(&note)
				details += fmt.Sprintf("\n支出\t%.2f\t\t%.2f\t%s", change, money, note)
			}
		case "4":
			for {
				fmt.Println("请输入y确认退出，n返回")
				fmt.Scanln(&operation)
				if operation == "y" || operation == "n" {
					break
				}

			}
			if operation == "y" {
				loop = false
			}
		default:
			fmt.Println("请输入正确的操作< 1~4 >")

		}
		if !loop {
			break
		}

	}

}
