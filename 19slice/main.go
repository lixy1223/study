//切片是引用类型

package main
import (
	"fmt"
)

//接收一个数，将斐波那契数列放到切片中
func fbn(n int) ([]uint64) {
	fbnSlice := make([]uint64,n)
	if n == 1 {
		fbnSlice[0] = 1
	} else {
		fbnSlice[0] = 1
		fbnSlice[1] = 1 
		for i := 2; i < n; i++ {
			fbnSlice[i] = fbnSlice[i-1] + fbnSlice[i - 2]
		}
	}
	return fbnSlice
}

func main() {
	fmt.Println("请输入一个正整数")
	var n int
	fmt.Scanln(&n)
	fbn := fbn(n)
	fmt.Println(fbn)

}