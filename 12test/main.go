//闭包

package main
import (
	"fmt"
)
func main() {
	n := 1
	f := func () int {
		n += 1
		return n
	}
	fmt.Println(f())
	fmt.Println(f())
	fmt.Println(n)
	fmt.Println(f())
	fmt.Println(f())
	n = 9
	fmt.Println(f())
	fmt.Println(f())
	
}