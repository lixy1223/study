package main

import "fmt"

type HeroNode struct {
	id   int
	name string
	nick string
	next *HeroNode
}

func insert(head *HeroNode, newHeroNode *HeroNode) {
	temp := head
	for {
		if temp.next == nil {
			break
		} else if temp.next.id > newHeroNode.id {
			break
		} else if temp.next.id == newHeroNode.id {
			fmt.Println("id重复，添加失败！")
			return
		}
		temp = temp.next
	}
	newHeroNode.next = temp.next
	temp.next = newHeroNode
	fmt.Println("添加成功！")
}

func listHeroNode(head *HeroNode) {
	temp := head
	if temp.next == nil {
		fmt.Println("这是一个空链表~")
		return
	}
	for {
		fmt.Printf("%v %v %v ==>", temp.next.id, temp.next.name,
			temp.next.nick)
		temp = temp.next
		if temp.next == nil {
			break
		}
	}
}

func deleteHeroNode(head *HeroNode, id int) {
	temp := head
	if temp.next == nil {
		fmt.Println("删除失败，这是一个空链表~")
		return
	}
	for {
		if temp.next.id == id {
			temp.next = temp.next.next
			break
		}
		temp = temp.next
		if temp.next == nil {
			fmt.Println("id不存在~")
			return
		}
	}
	fmt.Println("删除成功！id =", id)
}

func main() {

	//创建一个头结点
	head := &HeroNode{}

	//创建新的HeroNode
	hero1 := &HeroNode{
		id:   1,
		name: "宋江",
		nick: "及时雨",
	}

	hero2 := &HeroNode{
		id:   2,
		name: "卢俊义",
		nick: "玉麒麟",
	}

	hero3 := &HeroNode{
		id:   3,
		name: "林冲",
		nick: "豹子头",
	}

	//加入
	insert(head, hero3)
	insert(head, hero1)
	insert(head, hero2)
	insert(head, hero2)

	//显示
	listHeroNode(head)

	//删除
	fmt.Println()
	deleteHeroNode(head, 1)
	deleteHeroNode(head, 3)
	listHeroNode(head)
}
