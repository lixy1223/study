package main

import "fmt"

func selectSort(arr *[5]int) {
	for i := 0; i < len(arr)-1; i++ {
		maxIndex := i
		for j := i + 1; j < len(arr); j++ {
			if arr[maxIndex] < arr[j] {
				maxIndex = j
			}
		}
		if maxIndex != i {
			arr[i], arr[maxIndex] = arr[maxIndex], arr[i]
		}

	}
}

func main() {
	arr := [5]int{5, 86, 12, 456, 9}
	selectSort(&arr)
	fmt.Println(arr)
}
