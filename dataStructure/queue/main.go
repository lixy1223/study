package main

import (
	"errors"
	"fmt"
)

type Queue struct {
	maxSize int
	array   [5]int
	head    int
	tail    int
}

func (q *Queue) addQueue(val int) (err error) {
	if q.isFull() {
		return errors.New("Queue is full")
	}
	q.array[q.tail] = val
	q.tail = (q.tail + 1) % q.maxSize
	return
}

func (q *Queue) removeQueue() (err error) {
	if q.size() == 0 {
		return errors.New("Queue is empty")
	}
	fmt.Printf("取出了Queue[%d] = %d\n", q.head, q.array[q.head])
	q.head = (q.head + 1) % q.maxSize
	return
}

func (q *Queue) showQueue() {
	if q.size() == 0 {
		fmt.Println("Queue is empty")
	}
	tempHead := q.head
	for i := 0; i < q.size(); i++ {
		fmt.Printf("arr[%d] = %d\t", tempHead, q.array[tempHead])
		tempHead = (tempHead + 1) % q.maxSize
	}
	fmt.Println()
}

func (q *Queue) isFull() bool {
	return (q.tail+1)%q.maxSize == q.head
}

func (q *Queue) size() int {
	return (q.tail + q.maxSize - q.head) % q.maxSize
}

func main() {
	queue := &Queue{
		maxSize: 5,
		head:    0,
		tail:    0,
	}

	var key int
	var val int
	for {
		fmt.Println("请选择操作：")
		fmt.Println("1：添加数字到队列")
		fmt.Println("2：从队列中取出一个数字")
		fmt.Println("3：展示队列")
		fmt.Println("4：退出")
		fmt.Scanln(&key)
		switch key {
		case 1:
			fmt.Println("请输入想要加入队列的数：")
			fmt.Scanln(&val)
			err := queue.addQueue(val)
			if err != nil {
				fmt.Println("添加失败", err)
			} else {
				fmt.Println("添加成功")
			}
		case 2:
			err := queue.removeQueue()
			if err != nil {
				fmt.Println("取出数字失败", err)
			}
		case 3:
			queue.showQueue()
		case 4:
			return
		}
	}
}
