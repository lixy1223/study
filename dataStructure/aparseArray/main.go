package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
)

type ValNode struct {
	Row int
	Col int
	Val int
}

func main() {

	//创建一个原始数组
	var chessMap [11][11]int
	chessMap[1][2] = 1 //黑子
	chessMap[2][3] = 2 //蓝子

	//输出查看原始数组
	for _, v := range chessMap {
		for _, v2 := range v {
			fmt.Printf("%d\t", v2)
		}
		fmt.Println()
	}

	//转成稀疏数组

	var sparseArr []ValNode

	valNode := ValNode{
		Row: 11,
		Col: 11,
		Val: 0,
	} //初始化原始二维数组
	sparseArr = append(sparseArr, valNode)

	for i, v := range chessMap {
		for j, v2 := range v {
			if v2 != 0 {
				ValNode := ValNode{
					Row: i,
					Col: j,
					Val: v2,
				}
				sparseArr = append(sparseArr, ValNode)
			}
		}
	}
	//输出稀疏数组
	for _, v := range sparseArr {
		fmt.Printf("%v\n", v)
	}
	//存盘
	filePath := "e:/chessMap.data"
	newFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Printf("open newFile err=%v\n", err)
		return
	}
	defer newFile.Close()

	writer := bufio.NewWriter(newFile)
	for _, v := range sparseArr {
		content, err := json.Marshal(v)
		if err != nil {
			fmt.Printf("jsonMarshal failed, error: %v\n", err)
		}
		writer.Write(content)
	}
	writer.Flush()

	//恢复
	var chessMap2 [11][11]int

	var sparseArr2 []ValNode
	var sparseArr3 ValNode
	file, err := os.Open("e:/chessMap.data")
	if err != nil {
		fmt.Println("open file err=", err)
	}
	//关闭文件
	defer file.Close()

	//带缓存地读取文件
	reader := bufio.NewReader(file)
	//循环读取文件内容
	for {
		str, err := reader.ReadString('}')
		if err == io.EOF {
			break
		}
		if err := json.Unmarshal([]byte(str), &sparseArr3); err != nil {
			fmt.Println("Read file error =", err)
		}
		sparseArr2 = append(sparseArr2, sparseArr3)
	}

	for i, v := range sparseArr2 {
		if i != 0 {
			chessMap2[v.Row][v.Col] = v.Val
		}
	}
	fmt.Println("恢复二维数组")
	for _, v := range chessMap2 {
		for _, v2 := range v {
			fmt.Printf("%d\t", v2)
		}
		fmt.Println()
	}

}
