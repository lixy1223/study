package main

import (
	"fmt"
	"math/rand"
)

func insertSort(arr []int) {
	for i := 1; i < len(arr); i++ {
		insertVal := arr[i]
		insertIndex := i - 1
		for insertIndex >= 0 && insertVal > arr[insertIndex] {
			arr[insertIndex+1] = arr[insertIndex]
			insertIndex--
		}
		if insertIndex != i-1 {
			arr[insertIndex+1] = insertVal
		}
	}
}

func main() {

	var arr []int
	for i := 0; i < 15; i++ {
		arr = append(arr, rand.Intn(10))
	}

	fmt.Println("原始数组：", arr)
	insertSort(arr)

	fmt.Printf("插入排序后：")
	fmt.Println(arr)
}
