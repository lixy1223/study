package main

import (
	"fmt"
	"math/rand"
	"time"
)

func quickSort(a []int, left, right int) {
	l := left
	r := right
	pirot := a[(l+r)/2]
	for {
		for a[l] < pirot {
			l++
		}
		for a[r] > pirot {
			r--
		}
		if l >= r {
			break
		}
		a[l], a[r] = a[r], a[l]
		if a[l] == pirot {
			r--
		}
		if a[r] == pirot {
			l++
		}
	}
	if l == r {
		l++
		r--
	}
	if l < right {
		quickSort(a, l, right)
	}
	if r > left {
		quickSort(a, left, r)
	}
}

func main() {

	var arr []int
	for i := 0; i < 80000000; i++ {
		arr = append(arr, rand.Intn(9000000))
	}

	//fmt.Println(arr)
	start := time.Now().Unix()

	quickSort(arr, 0, len(arr)-1)
	end := time.Now().Unix()
	fmt.Printf("快速排序法耗时%d秒", end-start)
	//fmt.Println(arr)

}
