package main

import (
	"container/list"
	"fmt"
)

type HeroNode struct {
	id   int
	name string
	nick string
}

func main() {

	hero1 := &HeroNode{
		id:   1,
		name: "宋江",
		nick: "及时雨",
	}

	hero2 := &HeroNode{
		id:   2,
		name: "卢俊义",
		nick: "玉麒麟",
	}

	hero3 := &HeroNode{
		id:   3,
		name: "吴用",
		nick: "智多星",
	}

	hero4 := &HeroNode{
		id:   4,
		name: "公孙胜",
		nick: "入云龙",
	}
	fmt.Println("init list")
	heroList := list.New()

	//插入
	heroList.PushBack(hero2)

	fmt.Println(heroList)
	//头部添加
	heroList.PushFront(hero1)

	//尾部添加
	mark := heroList.PushBack(hero4)
	fmt.Println(heroList)

	//指定位置插入
	heroList.InsertBefore(hero3, mark)

	//删除
	heroList.Remove(mark)
	// 遍历
	for i := heroList.Front(); i != nil; i = i.Next() {
		fmt.Println(i.Value)
	}
}
