package main

import "fmt"

func SetWay(m *[8][7]int, i, j int) bool {
	//终点
	if m[6][5] == 2 {
		return true
	} else {
		if m[i][j] == 0 {
			m[i][j] = 2 //寻路，下右上左
			if SetWay(m, i+1, j) {
				return true
			} else if SetWay(m, i, j+1) {
				return true
			} else if SetWay(m, i-1, j) {
				return true
			} else if SetWay(m, i, j-1) {
				return true
			} else {
				m[i][j] = 3 //不通
				return false
			}
		} else {
			return false //没有路了
		}
	}
}

func main() {
	//创建一个二维数组，模拟迷宫
	//规则
	//1. 如果元素的值为1 ，就是墙
	//2. 如果元素的值为0, 是没有走过的点
	//3. 如果元素的值为2, 是一个通路
	//4. 如果元素的值为3， 是走过的点，但是走不通
	var myMap [8][7]int

	//把地图的边界设置为1
	for i := 0; i < 7; i++ {
		myMap[0][i] = 1
		myMap[7][i] = 1
	}
	for i := 0; i < 8; i++ {
		myMap[i][0] = 1
		myMap[i][6] = 1
	}

	myMap[4][1] = 1
	myMap[3][2] = 1
	myMap[2][3] = 1
	myMap[2][2] = 1
	myMap[6][4] = 1

	//输出地图
	for i := 0; i < 8; i++ {
		for j := 0; j < 7; j++ {
			fmt.Print(myMap[i][j], " ")
		}
		fmt.Println()
	}

	//使用测试
	SetWay(&myMap, 1, 1)
	fmt.Println("探测完毕....")
	//输出地图
	for i := 0; i < 8; i++ {
		for j := 0; j < 7; j++ {
			fmt.Print(myMap[i][j], " ")
		}
		fmt.Println()
	}

}
