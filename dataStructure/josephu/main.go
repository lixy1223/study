package main

import (
	"container/ring"
	"fmt"
)

func main() {

	r1 := ring.New(20)
	for i := 1; i <= 20; i++ {
		r1.Value = i
		r1 = r1.Next()
	}

	for {
		r1 = r1.Move(2)
		r2 := r1.Unlink(1)
		fmt.Println(r2.Value)
		if r1.Next() == r2 {
			break
		}
	}
}
