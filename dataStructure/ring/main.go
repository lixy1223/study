package main

import (
	"container/ring"
	"fmt"
)

func main() {
	// 创建一个循环双向链表，并初始化
	r1 := ring.New(10)
	for i := 1; i <= 8; i++ {
		r1.Value = i * 10
		r1 = r1.Next()
	}
	r1.Value = 100
	r1.Next().Value = "hello world"

	//对r中的每个元素执行函数（顺序）
	fmt.Printf("r1: ")
	r1.Do(func(i interface{}) {
		fmt.Printf("%v\t", i)
	})
	fmt.Println()
	//获取环形链表中元素的个数r.Len()
	fmt.Println(r1.Len())

	//func (r *Ring) Move(n int) *Ring 返回移动n个节点后的节点指针
	r2 := r1.Move(3)
	fmt.Printf("r2: ")
	r2.Do(func(i interface{}) {
		fmt.Printf("%v\t", i)
	})
	fmt.Println()

	//func (r *Ring) Unlink(n int) *Ring
	//删除链表中n % r.Len()个元素，从r.Next()开始删除。
	//如果n % r.Len() == 0，不修改r。
	//返回删除的元素构成的链表，r不能为空。
	r3 := r1.Unlink(2)
	fmt.Printf("r3: ")
	r3.Do(func(i interface{}) {
		fmt.Printf("%v\t", i)
	})
	fmt.Println()

	fmt.Printf("r1: ")
	r1.Do(func(i interface{}) {
		fmt.Printf("%v\t", i)
	})
	fmt.Println()

	//连接两个链表
	// r4 := r1.Link(r3)
	// fmt.Printf("r4: ")
	// r4.Do(func(i interface{}) {
	// 	fmt.Printf("%v\t", i)
	// })
	// fmt.Println()
	r1.Link(r3)
	fmt.Printf("r3: ")
	r3.Do(func(i interface{}) {
		fmt.Printf("%v\t", i)
	})
	fmt.Println()
}
