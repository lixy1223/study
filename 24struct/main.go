package main

import (
	"fmt"
)

//结构体默认为值类型
type Person struct {
	Name string
	Age  int
	// 	Scores []float64
	// 	//指针，切片和map是引用类型，切片和map使用前需make
	// 	ptr *int
	// 	slice []int
	// 	map1 map[string]string
}

func main() {

	var p1 Person
	fmt.Println(p1) //{ 0 [] <nil> [] map[]}

	// p1.slice = make([]int, 10)
	// p1.slice[0] = 100
	// p1.map1 = make(map[string]string)
	// p1.map1["key1"] = "ok1"
	// fmt.Println(p1)

	p2 := Person{"tom", 18}
	fmt.Println(p2)

	p3 := new(Person) //= var p3 *Person = new(Person) , 指针
	p3.Name = "John"
	p3.Age = 20
	fmt.Println(*p3)

}
