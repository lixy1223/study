package unitconv

import "fmt"

type Foot float64  // 英尺
type Meter float64 //米
type Pound float64 //磅
type Kilo float64  //千克

// String方法在输出时自动调用
func (f Foot) String() string  { return fmt.Sprintf("%gft", f) }
func (m Meter) String() string { return fmt.Sprintf("%gm", m) }
func (k Kilo) String() string  { return fmt.Sprintf("%gkg", k) }
func (p Pound) String() string { return fmt.Sprintf("%glb", p) }
