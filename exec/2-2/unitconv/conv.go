package unitconv

// 英尺转米
func FToM(f Foot) Meter { return Meter(f * 0.3048) }

// 磅转千克
func PToK(p Pound) Kilo { return Kilo(p * 0.45359237) }
