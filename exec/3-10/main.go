package main

import (
	"bytes"
	"fmt"
	"strings"
)

func main() {
	fmt.Println("\uFFFD")
	fmt.Println(commar("-02345612.1245"))
	fmt.Println(isSame("-02345612.1245", "-02345621.5421"))
}

func commar(s string) string {
	var b bytes.Buffer
	var symbol byte
	// 处理符号
	if s[0] == '-' || s[0] == '+' {
		symbol = s[0]
		s = s[1:]
		b.WriteByte(symbol)
	}
	if len(s) <= 3 {
		return s
	}

	// 分割小数部分
	arr := strings.Split(s, ".")
	s = arr[0]

	for i := 0; i < len(s); i += 3 {
		if i+3 < len(s) {
			b.WriteString(s[i : i+3])
			b.WriteString(",")
		} else {
			b.WriteString(s[i:])
		}
	}

	// 处理小数部分
	if len(arr) > 1 {
		b.WriteString(".")
		b.WriteString(arr[1])
	}
	return b.String()
}

func isSame(s1, s2 string) bool {
	if len(s1) != len(s2) {
		return false
	}
	m1 := make(map[rune]int)
	m2 := make(map[rune]int)
	for _, v := range s1 {
		m1[v]++
	}
	for _, v := range s2 {
		m2[v]++
	}
	for i, v := range m1 {
		if v != m2[i] {
			return false
		}
	}
	return true
}
