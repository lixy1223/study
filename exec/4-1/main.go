package main

import (
	"crypto/sha256"
	"crypto/sha512"
	"flag"
	"fmt"
)

func main() {

	var hashMethod = flag.String("s", "sha256", "指定哈希算法")
	var input = flag.String("i", "", "指定想要hash的字符串")
	flag.Parse()
	switch *hashMethod {
	case "sha256":
		fmt.Printf("%x\n", sha256.Sum256([]byte(*input)))
	case "sha384":
		fmt.Printf("%x\n", sha512.Sum384([]byte(*input)))
	case "sha512":
		fmt.Printf("%x\n", sha512.Sum512([]byte(*input)))

	}
	s1 := sha256.Sum256([]byte("x"))
	s2 := sha256.Sum256([]byte("X"))
	fmt.Println(count(s1, s2))

}

func count(s1, s2 [32]byte) int {
	count := 0
	for i := 0; i < len(s1); i++ {
		for j := 1; j <= 8; j++ {
			if s1[i]>>j != s2[i]>>j {
				count++
			}
		}
	}
	return count
}
