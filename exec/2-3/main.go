package main

import "fmt"

// pc[i] is the population count of i.
var pc [256]byte

// 把0~255中每个数的2进制中1的数目放入数组中
func init() {
	for i := range pc {
		pc[i] = pc[i/2] + byte(i&1)
	}
}

func main() {
	i := PopCount(511)
	fmt.Println(i, "~")
	i = PopCountByRight(511)
	fmt.Println(i, ".")
	fmt.Println(PopCountByClearing(511))
}

// PopCount returns the population count (number of set bits) of x.
func PopCount(x uint64) (n int) {
	for i := 0; i < 8; i++ {
		n += int(pc[byte(x>>(i*8))])
	}
	return
}

// 移位法
func PopCountByRight(x uint64) (n int) {
	for i := 0; i < 64; i++ {
		if x&1 != 0 {
			n++
		}
		x >>= 1
	}
	return
}

// 快速法，循环n次
func PopCountByClearing(x uint64) (n int) {
	for x != 0 {
		x = x & (x - 1)
		n++
	}
	return
}
