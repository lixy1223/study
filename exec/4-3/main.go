package main

import (
	"fmt"
	"unicode"
)

func main() {
	a := [5]int{1, 2, 3, 4, 5}
	reverse(&a)
	fmt.Println(a)
	b := []int{1, 2, 3, 4, 5}
	b = rotate(b)
	fmt.Println(b)
	s := []string{"a", "b", "c", "c", "d", "e", "f", "g", "h"}
	s = deleteRepeat(s)
	fmt.Println(s)
	c := []byte("    你好   世   界  ")
	c = reverse1(c)
	fmt.Println(string(c))
	c = deleteRepeatSpace(c)
	fmt.Println(string(c))
}

// 反转数组
func reverse(s *[5]int) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}

// 反正切片
func reverse1(b []byte) []byte {
	s := []rune(string(b))
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
	return []byte(string(s))
}

func rotate(s []int) []int {
	s2 := make([]int, len(s))
	j := 0
	for i := len(s) - 1; i >= 0; i-- {
		s2[j] = s[i]
		j++
	}
	return s2

}

// 删除重复字符串
func deleteRepeat(s []string) []string {
	j := 0
	temp := s[0]
	for i := 1; i < len(s); i++ {
		if s[i] != temp {
			j++
			s[j] = s[i]
			temp = s[i]
		}
	}
	return s[:j+1]
}

// 删除重复空格
func deleteRepeatSpace(s []byte) []byte {
	j := 0
	temp := s[0]
	for i := 1; i < len(s); i++ {
		if s[i] != temp {
			j++
			s[j] = s[i]
			temp = s[i]
		} else if s[i] == temp && !unicode.IsSpace(rune(s[i])) {
			j++
			s[j] = s[i]
			temp = s[i]
		}
	}
	return s[:j+1]
}
