package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"unicode"
)

func main() {
	mapv := make(map[string]int)

	input := bufio.NewScanner(os.Stdin)
	input.Split(bufio.ScanWords)
	for input.Scan() {

		str := input.Text()
		mapv[str]++
	}

	fmt.Printf("The length %d\n", len(mapv))
	for k, v := range mapv {
		fmt.Printf("%s %d\n", k, v)
	}
	charCount("12315sf0. 0dxgfb!#&*@^$*&gs")
}
func charCount(s string) {
	var letterCount, numberCount, otherCount int
	// 输入
	//将 s 作为 输入
	in := bufio.NewReader(strings.NewReader(s))
	for {
		// 以 rune 为单位读取
		r, n, err := in.ReadRune()

		if err == io.EOF {
			break
		}
		if err != nil {
			fmt.Fprintf(os.Stderr, "charcount: %v", err)
			os.Exit(1)
		}
		// 无效码点
		if r == unicode.ReplacementChar && n == 1 {
			//invalid++
			continue
		}
		if unicode.IsNumber(r) {
			numberCount++
			fmt.Printf("%c is number\t", r)
		} else if unicode.IsLetter(r) {
			letterCount++
			fmt.Printf("%c is letter\t", r)
		} else {
			otherCount++
		}

	}
	fmt.Printf("letter: %v,	 number,%v	 other: %v", letterCount, numberCount, otherCount)
}
