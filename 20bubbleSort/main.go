package main
import (
	"fmt"
)

//冒泡排序
func bubbleSort (srr *[10]int) {
	var i int
	for i = len(srr) - 1; i >= 0; i-- {
		for j := 0; j < i; j++ {
			if srr[j] > srr[j + 1] {
				srr[j + 1], srr[j] = srr[j], srr[j+1]
			}
		}

	}
	fmt.Println(*srr)
}
func main() {
	srr := [10]int{8, 5, 9, 34, 4, 76, 12, 0, 435, 98}
	bubbleSort(&srr)
}