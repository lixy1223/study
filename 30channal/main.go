package main

import (
	"fmt"
	"time"
)

//向 intChan放入 1-800000个数
func putNum(intChan chan int) {

	for i := 1; i <= 800000; i++ {
		intChan <- i
	}

	//关闭intChan
	close(intChan)
}

// 从 intChan取出数据，并判断是否为素数,如果是，就放入到primeChan
func primeNum(intChan chan int, primeChan chan int, exitChan chan bool) {

	//使用for 循环
	// var num int
	var flag bool //
	for {
		num, ok := <-intChan
		//intChan 取不到
		if !ok {
			break
		}
		flag = true //假设是素数
		//判断num是不是素数
		for i := 2; i < num; i++ {
			if num%i == 0 { //说明该num不是素数
				flag = false
				break
			}
		}

		if flag {
			//将这个数就放入到primeChan
			primeChan <- num
		}
	}

	fmt.Println("有一个primeNum 协程因为取不到数据，退出")
	//向 exitChan 写入true
	exitChan <- true

}

func main() {

	intChan := make(chan int, 1000)     //放入数据
	primeChan := make(chan int, 200000) //放入结果
	//标识退出的管道
	exitChan := make(chan bool, 24)

	start := time.Now().Unix()

	//开启一个协程，向 intChan放入 1-800000个数
	go putNum(intChan)
	//开启6个协程，从 intChan取出数据，并判断是否为素数,如果是，就放入到primeChan
	//由于只有6个逻辑处理器，开启更多的协程速度也不会有明显的提升(并行与并发)
	for i := 0; i < 6; i++ {
		go primeNum(intChan, primeChan, exitChan)
	}

	go func() {
		for i := 0; i < 6; i++ {
			<-exitChan
		}

		end := time.Now().Unix()
		fmt.Println("使用协程耗时=", end-start)
		//取出所有结果后，关闭 primeChan
		close(primeChan)
	}()
	// time.Sleep(time.Second * 20)
	// fmt.Printf("800000以内的素数一共有%d个\n", len(primeChan))
	//遍历primeChan ,把结果取出
	for {
		res, ok := <-primeChan
		if !ok {
			break
		}
		//将结果输出
		fmt.Printf("素数=%d\n", res)
	}

	fmt.Println("main线程退出")

}

// func main() {

// 	write()
// 	read()
// 	time.Sleep(time.Second * 10)

// type Person struct {
// 	Name    string
// 	Age     int
// 	Address string
// }
// p1 := Person{"tom", 1, "a"}
// p2 := Person{"tom", 2, "a"}
// p3 := Person{"tom", 3, "a"}

// //声明
// allChan := make(chan interface{}, 4)
// //写入
// allChan <- 10
// allChan <- p1
// allChan <- p2
// allChan <- p3
// //取出
// v := <-allChan
// fmt.Println(v)

// close(allChan) //关闭管道，只能读取，不能写入，若不关闭，则遍历时会报deadlock!
// for v := range allChan {
// 	i := 1
// 	i++
// 	v = v.(Person)
// 	fmt.Printf("p%d=%v\n", i, v)
// }

// }
