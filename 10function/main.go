//函数 
//定义 
// func 函数名 (形参列表) (返回值列表){
// 	执行语句
// 	return 返回值列表
// }
package main
import (
	"fmt"
)
//定义一个函数，输入两个数字和一个运算符，进行计算
func cal(n1 float64, n2 float64, operator string)(res float64){
	switch operator {
	case "+" :
		res = n1 + n2
	case "-" :
		res = n1 - n2
	case "*" :
		res = n1 * n2
	case "/" :
		res = n1 / n2
	default:
		fmt.Println("符号错误，请输入+，-，*，/中的一个！")
	}
	return res
}


func main(){
	var n1 float64
	var n2 float64
	var operator string
	var res float64
	fmt.Println("请输入第一个数")
	fmt.Scanln(&n1)
	fmt.Println("请输入第二个数")
	fmt.Scanln(&n2)
	fmt.Println("请输入运算符")
	fmt.Scanln(&operator)
	res = cal(n1 , n2 , operator)
	fmt.Printf("%f %s %f = %f", n1, operator,n2, res)
}