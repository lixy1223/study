//练习，使用函数交换n1,n2的值


package main
import (
	"fmt"
)
func swap(m1, m2 int)(n2, n1 int){
	n1, n2 = m1, m2 
	return 
}
func main(){
	var n1 int
	var n2 int
	fmt.Println("请输入两个整数")
	fmt.Scanln(&n1, &n2)
	n1, n2 = swap(n1, n2)
	fmt.Println(n1, n2)
}