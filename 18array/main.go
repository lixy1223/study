package main
import (
	"fmt"
)

func test1(arr [3]int) {
	arr[0] = 10
}

func test2(arr *[3]int) {
	arr[0] = 10
}
func main() {
	arr := [3]int{1, 2, 3}
	test1(arr)
	fmt.Println(arr)  //数组是值类型，进行值拷贝
	//输出结果 [1 2 3]
	test2(&arr)  
	fmt.Println(arr)  
	//输出结果 [10 2 3]

	//创建并打印26个大写字母的数组，使用for循环
	var chars [26]byte
	for i := 0 ; i <= 25 ;i ++ {
		chars[i] = 'A' + byte(i)
		fmt.Printf("%c",chars[i])
	}
	fmt.Printf("\n")

	//求一个数组的最大值及其下标
	var intArr =[...]int{1, 9, 7, 67, 8, 0}
	maxVal := intArr[0]
	maxValIndex := 0
	for i :=0; i <len(intArr); i++ {
		if intArr[i] > maxVal {
			maxVal = intArr[i]
			maxValIndex = i
		}
	}
	fmt.Printf("最大值是%v，它的下标是%v",maxVal,maxValIndex)
	fmt.Printf("\n")

	//for-range求数组的和和平均值
	sum := 0
	for _, val := range intArr {
		sum += val
	}
	fmt.Printf("和是%v,平均值是%v", sum, float64(sum)/float64(len(intArr)))
}