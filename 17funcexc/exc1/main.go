//练习  循环打印输入月份的天数

package main

import (
	"fmt"
)

func main() {

	for {
		var year, month int
		fmt.Println("请输入年")
		fmt.Scanln(&year)
		fmt.Println("请输入月")
		fmt.Scanln(&month)
		if month > 0 && month < 12 {
			if (year % 4 == 0 && year % 100 != 0 ) || year % 400 == 0 {
				switch month {
				case 1, 3, 5, 7, 8, 10, 12:
					fmt.Println(year,"年",month,"月有31天")
				case 2:
					fmt.Println(year,"年",month,"月有29天")
				default:
					fmt.Println(year,"年",month,"月有30天")
				}
			} else{
				switch month {
				case 1, 3, 5, 7, 8, 10, 12:
					fmt.Println(year,"年",month,"月有31天")
				case 2:
					fmt.Println(year,"年",month,"月有28天")
				default:
					fmt.Println(year,"年",month,"月有30天")
				}
			}
		} else {
			fmt.Println("请输入正确的月份")
		}
	}
}