//练习 猜数游戏

package main
import (
	"fmt"
	"math/rand"
	"time"
)

func game() {
	var i, n, a int 
	rand.Seed(time.Now().UnixNano())
	a = rand.Intn(11)
	for i = 1; i <= 100 ; i++ {
		fmt.Println("请猜一个数字（0~10）")
		fmt.Scanln(&n)
	
		if n == a {
			fmt.Println("猜对啦")
			break  
		}	
	}
	switch i {
	case 1:
		fmt.Println("你真是个天才")
	case 2, 3:
		fmt.Println("你很聪明，快赶上我了")
	case 4, 5, 6, 7, 8, 9:
		fmt.Println("一般般")
	default:
		fmt.Println("可算猜对啦！")
	}
}

func main() {
	
	game()
}