package main

import (
	"fmt"
	"sort"
)

func main() {

	var heroes map[string]string
	//map在使用前需要先make，或者在声明时直接make或赋值
	heroes = make(map[string]string, 10)
	heroes["first"] = "宋江"
	heroes["second"] = "卢俊义"
	heroes["third"] = "吴用"
	fmt.Println(heroes)

	//map的查找
	val, ok := heroes["first"]
	if ok {
		fmt.Printf("有first key, 值为%v\n", val)
	} else {
		fmt.Printf("没有")
	}

	//map的删除
	delete(heroes, "third")

	//增添或修改
	heroes["third~"] = "吴用~"
	fmt.Println(heroes)

	//遍历 
	for k, v := range heroes {
		fmt.Printf("k = %v, v = %v\n", k, v)
	}

	//长度(key的个数)
	fmt.Println(len(heroes)) //3

	//切片
	var hero []map[string]string //声明切片
	hero = make([]map[string]string, 3)  //初始化切片
	hero[0] = make(map[string]string, 2)  //初始化map
	hero[0]["name"] = "孙悟空"  //赋值
	hero[0]["age"] = "500"

	hero[1] = make(map[string]string, 2)  //初始化map
	hero[1]["name"] = "猪八戒"  //赋值
	hero[1]["age"] = "800"

	hero[2] = make(map[string]string, 2)  //初始化map
	hero[2]["name"] = "沙僧"  //赋值
	hero[2]["age"] = "900"

	//使用append在切片中增添map
	var newHero map[string]string
	newHero = make(map[string]string)
	newHero["name"] = "唐僧"
	newHero["age"] = "40"
	hero = append(hero, newHero)
	fmt.Println(hero)

	//排序输出
	map1 := make(map[int]int)
	map1[6] = 1
	map1[9] = 10
	map1[2] = 15
	map1[3] = 20
	fmt.Println(map1)
	//若不排序，遍历时是随机的
	for k, v := range map1 {
		fmt.Printf("map1[%v] = %v\t", k, v)
	}
	fmt.Printf("\n")
	
	//将key放到切片中，对切片进行排序
	var keys []int 
	for k, _ := range map1 {
		keys = append(keys, k)
	}  
	sort.Ints(keys) //递增排序
	for _, v := range keys {
		fmt.Printf("map1[%v] = %v\t", v, map1[v])
	}
	fmt.Printf("\n")
}