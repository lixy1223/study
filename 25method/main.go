package main

import (
	"fmt"
)

//编写结构体MethodUtils和方法，调用方法打印m*n的矩形

type MethodUtils struct {
}

func (me *MethodUtils) Print(m, n int) {
	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			fmt.Print("*")
		}
		fmt.Printf("\n")
	}
}

func main() {

	var methods MethodUtils
	methods.Print(6, 2)
}
