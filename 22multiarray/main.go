package main
import (
	"fmt"
)

//二维数组

func main() {

	//使用一个二维数组接收3个班*5名学生的成绩 求每个班级的平均分以及所有班级平均分
	var arr [3][5]float64
	for i := 0; i < 3; i++ {
		for j := 0; j < 5; j++ {
			fmt.Printf("请输入第%d个班第%d名学生的成绩", i + 1, j + 1)
			fmt.Scanln(&arr[i][j])
		}

		
	}
	fmt.Println(arr)

	total := 0.0
	for i := 0; i < 3; i++ {
		sum := 0.0
		for j := 0; j < 5; j++ {
			
			sum += arr[i][j]
		}
		fmt.Printf("%d班学生的平均分是%.2f\n", i + 1, sum/5)
		total += sum
	}
	fmt.Printf("三个班学生的平均分是%.2f\n", total/15)		

}