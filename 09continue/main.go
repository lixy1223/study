//continue 跳过后续语句，继续执行下一次迭代

package main
import (
	"fmt"
)
func main() {
//输出【0，100】间的奇数
	// for i := 0 ; i <= 100 ; i++ {
	// 	if i % 2 == 0 {
	// 		continue
	// 	}
	// 	fmt.Println(i)
	// }
//练习  100000,大于50000时-5%，否则-1000，能减多少次
	var money float64 = 100000
	var i int = 1
		for {
			money *= 0.95
			if money <= 50000 {
				break
			}
			i++
		}
	fmt.Println(int(money)/1000+i)
}