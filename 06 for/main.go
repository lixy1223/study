// for循环
// go没有while和do...while

package main
import (
	"fmt"
)
func main() {
	// var i int = 0
	// for i < 10{
	// 	fmt.Println("hello",i)
	// 	i ++
	// }
	
	for i := 1 ; i <= 10 ; i++ {
	//循环变量初始化；循环条件；循环变量迭代 
		fmt.Println("hello",i)
	}


	//字符串遍历(传统方式)
	// var str string = "hello, world!你好"
	// for i := 0; i < len(str); i++ {//按字节遍历，如果str含中文会乱码（汉字3字节）
	// 	fmt.Printf("%c \n",str[i])
	// }
	var str string = "hello, world!你好"
	str2 := []rune(str)//转切片后汉字不再乱码
	for i := 0; i < len(str2); i++ {
		fmt.Printf("%c \n",str2[i])
	}
	//字符串遍历(-for-range)
	str = "你好，世界！"
	for index, val := range str{
		fmt.Printf("%d, %c\n",index,val)
	}

}