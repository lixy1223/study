package main

import (
	"fmt"
	"runtime"
	"strconv"
	"time"
)

func test() {
	for i := 1; i <= 10; i++ {
		fmt.Println("hello, world test" + strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}
func test2() {
	for i := 1; i <= 20; i++ {
		fmt.Println("hello, world test2" + strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}

func main() {

	// go test() //开启一个协程

	// go test2() //主线程执行完毕后协程也会退出

	// for i := 1; i <= 10; i++ {
	// 	fmt.Println("hello, world" + strconv.Itoa(i))
	// 	time.Sleep(time.Second)
	// }

	cpuNum := runtime.NumCPU() //获取当前系统CPU数量
	fmt.Println("cpuNum =", cpuNum)
}
