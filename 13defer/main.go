package main

import (
	"fmt"
)
func test() {
	// defer用于释放资源，在函数中使用完毕后关闭
	// file = openfile(文件名)
	// defer file.Close()
	// 其它代码
}
func sum(n1, n2 int) int  {
	//函数执行完毕后，按照先入后出的顺序执行defer后的语句
	defer fmt.Println("ok1 n1 =", n1)
	defer fmt.Println("ok2 n2 =", n2)
	res := n1 + n2
	fmt.Println("ok3 res =", res)
	//test
	var a *int = &n1
	var b *int = &n2
	defer fmt.Println("ok1 n1~ =", *a)
	defer fmt.Println("ok2 n2~ =", *b)
	n1++
	n2++
	defer fmt.Println("ok1 n1 =", n1)
	defer fmt.Println("ok2 n2 =", n2)
	defer fmt.Println("ok1 n1~~ =", *a)
	defer fmt.Println("ok2 n2~~ =", *b)
	return res
}

func main() {
	res := sum(10, 20)
	fmt.Println("res =", res)
}
// 输出结果
// ok3 res = 30
// ok2 n2~~ = 21
// ok1 n1~~ = 11
// ok2 n2 = 21
// ok1 n1 = 11
// ok2 n2~ = 20
// ok1 n1~ = 10
// ok2 n2 = 20
// ok1 n1 = 10
// res = 30
