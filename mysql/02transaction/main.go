package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql" //init()
)

var db *sql.DB //连接池

func initDB() (err error) {
	// 数据库信息
	//用户名：密码@tcp（ip:端口）/数据库名
	dsn := "root:2392073222@tcp(127.0.0.1:3306)/go01"
	//连接数据库
	db, err = sql.Open("mysql", dsn) //不会校验用户名和密码是否正确,只校验dsn格式
	if err != nil {
		return
	}
	err = db.Ping() //Ping检查与数据库的连接是否仍有效，如果需要会创建连接。
	if err != nil {
		return
	}
	//设置连接池最大连接数
	db.SetMaxOpenConns(10)
	//设置最大空闲连接数
	db.SetMaxIdleConns(5)
	return
}

func transaction() {
	//开启事务
	tx, err := db.Begin()
	if err != nil {
		fmt.Printf("Begin failed, error: %v\n", err)
		return
	}
	//执行多个sql操作
	sqlStr1 := `update user set age = age + 10 where id = 9`
	sqlStr2 := `update user set age = age - 10 where id = 10`
	_, err = tx.Exec(sqlStr1)
	if err != nil {
		//回滚
		tx.Rollback()
		fmt.Printf("exec sqlStr1 failed, rollback!\n")
		return
	}
	_, err = tx.Exec(sqlStr2)
	if err != nil {
		tx.Rollback()
		fmt.Printf("exec sqlStr2 failed, rollback!\n")
		return
	}
	//执行成功，提交
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		fmt.Printf("commit failed, rollback\n")
		return
	}
	fmt.Printf("事务执行完毕！")
}

func main() {
	err := initDB()
	if err != nil {
		fmt.Printf("init DB failed, err: %v", err)
	}
	fmt.Println("连接数据库成功")

	transaction()

}
