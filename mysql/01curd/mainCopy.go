package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql" //init()
)

var db *sql.DB //连接池

func initDB() (err error) {
	// 数据库信息
	//用户名：密码@tcp（ip:端口）/数据库名
	dsn := "root:2392073222@tcp(127.0.0.1:3306)/go01"
	//连接数据库
	db, err = sql.Open("mysql", dsn) //不会校验用户名和密码是否正确,只校验dsn格式
	if err != nil {
		return
	}
	err = db.Ping() //Ping检查与数据库的连接是否仍有效，如果需要会创建连接。
	if err != nil {
		return
	}
	//设置连接池最大连接数
	db.SetMaxOpenConns(10)
	//设置最大空闲连接数
	db.SetMaxIdleConns(5)
	return
}

//查询一行
func QueryRow(id int) {
	var u1 user
	//写一条查询单行的sql语句
	sqlStr := `select id,name,age from user where id=?;`
	//执行
	//db.QueryRow(sqlStr, 1).Scan(&u1.id, &u1.name, &u1.age)
	rowObj := db.QueryRow(sqlStr, id)      //返回*sql.Row
	rowObj.Scan(&u1.id, &u1.name, &u1.age) //Scan方法中包含释放连接的操作，一定要记得调用，否则会一直占用连接
	fmt.Printf("u1:%#v\n", u1)
}

//查询多行
func QueryRows(n int) {
	var u1 user
	//sql语句
	sqlStr := `select id,name,age from user where id > ?;`
	//执行
	rows, err := db.Query(sqlStr, n)
	if err != nil {
		fmt.Printf("exec %s query failed, error: %v\n", sqlStr, err)
		return
	}
	//关闭rows
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&u1.id, &u1.name, &u1.age)
		if err != nil {
			fmt.Printf("scan failed, error:%v\n", err)
			return
		}
		fmt.Printf("u1:%#v\n", u1)
	}
}

//插入数据
func Insert() {
	sqlStr := `insert into user(name, age) values("小明", 18)`
	res, err := db.Exec(sqlStr)
	if err != nil {
		fmt.Printf("exec %s insert failed, error: %v\n", sqlStr, err)
		return
	}
	id, err := res.LastInsertId()
	if err != nil {
		fmt.Printf("get id failed, error: %v\n", err)
		return
	}
	fmt.Println("insert id:", id)
}

//更新数据
func UpdateRow(newName string, id int) {
	sqlStr := `update user set name=? where id = ?`
	res, err := db.Exec(sqlStr, newName, id)
	if err != nil {
		fmt.Printf("update failed, error: %v\n", err)
		return
	}
	i, err := res.RowsAffected()
	if err != nil {
		fmt.Printf("get RowsAffected failed, error: %v\n", err)
		return
	}
	fmt.Printf("update success, affected rows: %v\n", i)
}

//删除数据
func DeleteRow(id int) {
	sqlStr := `delete from user where id = ?`
	res, err := db.Exec(sqlStr, id)
	if err != nil {
		fmt.Printf("delete failed, error: %v\n", err)
		return
	}
	i, err := res.RowsAffected()
	if err != nil {
		fmt.Printf("get RowsAffected failed, error: %v\n", err)
		return
	}
	fmt.Printf("delete success, affected rows: %v\n", i)

}

//预处理方式插入多条数据
func PrepareInsert() {
	sqlStr := `insert into user(name, age) values(?, ?)`
	stmt, err := db.Prepare(sqlStr)
	if err != nil {
		fmt.Printf("prepare failed, error: %v\n", err)
		return
	}
	defer stmt.Close()
	var m = map[string]int{
		"小灰": 20,
		"小红": 21,
		"小白": 22,
	}
	for n, a := range m {
		stmt.Exec(n, a)
	}
}

type user struct {
	id   int
	name string
	age  int
}

func main() {
	err := initDB()
	if err != nil {
		fmt.Printf("init DB failed, err: %v", err)
	}
	fmt.Println("连接数据库成功")

	// QueryRow(2)
	// QueryRows(0)
	// insert()
	// updateRow("小黄", 6)
	// deleteRow(7)
	PrepareInsert()
	QueryRows(0)

}
