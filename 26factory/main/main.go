package main

import (
	"fmt"
	"study/26factory/model"
)

func main() {
	var stu = model.NewStudent("Tom")
	stu.SetAge(18)
	stu.SetScore(60.5)
	fmt.Println(*stu)
	fmt.Println("age =", stu.GetAge(), "score =", stu.GetScore())
}
