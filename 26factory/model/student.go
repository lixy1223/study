package model

import "fmt"

type student struct {
	Name  string
	age   int
	score float64
}

//由于student首字母是小写的，只能在model包中使用，可通过工厂模式解决
func NewStudent(name string) *student {
	return &student{
		Name: name,
	}
}

//设置年龄
func (s *student) SetAge(age int) {
	if age > 0 {
		s.age = age
	} else {
		fmt.Println("Age must be greater than 0.")
	}
}

//获取年龄
func (s *student) GetAge() int {
	return s.age
}

func (s *student) SetScore(score float64) {
	if score >= 0 && score <= 100 {
		s.score = score
	} else {
		fmt.Println("输入分数不正确")
	}
}

//获取分数
func (s *student) GetScore() float64 {
	return s.score
}
