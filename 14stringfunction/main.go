package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	//统计字符串长度(字节)
	str := "hello你好"
	fmt.Println(len(str))  //11

	//字符串转切片遍历 (不转的话中文会出现乱码)
	s := []rune(str)
	for i := 0; i < len(s); i++ {
		fmt.Printf("%c\n", s[i])
	}

	//字符串转整数
	n, err := strconv.Atoi("123") //返回一个int和error
	fmt.Println(n, err)  //123 <nil>

	//整数转字符串
	str2 := strconv.Itoa(123)
	fmt.Println(str2)

	//字符串转切片[]byte
	var bytes = []byte(str)
	fmt.Printf("%c\n", bytes)

	//[]byte转字符串
	str3 := string([]byte{97, 98, 99})
	fmt.Println(str3)  //abc

	//10进制转2，8，16进制，返回字符串
	str4 := strconv.FormatInt(123, 2)
	fmt.Println(str4)

	//查找子串是否在指定的字符串中
	fmt.Println(strings.Contains(str,"h")) //true

	//统计字符串中包含几个指定的子串
	fmt.Println(strings.Count(str,"l")) //2

	//...


}