package main

import (
	"fmt"
	"time"
)

func main() {

	//获取当前时间
	now := time.Now()
	fmt.Println(now)

	//格式化当前时间
	fmt.Printf("%d-%d-%d %d:%d:%d\n", now.Year(), now.Month(), now.Day(), 
	now.Hour(), now.Minute(), now.Second())

	fmt.Printf(now.Format("2006-01-02 15:04:05"))
	fmt.Println()

	//打印1-10，间隔0.1秒
	for i := 1; i <= 10; i++ {
		fmt.Println(i)
		time.Sleep(time.Second/10)
	}

	//获取当前unix或unixnano时间戳，可用于获取随机种子
	fmt.Printf("%v, %v", now.Unix(), now.UnixNano())

}