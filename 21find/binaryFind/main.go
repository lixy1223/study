package main
import (
	"fmt"
)

//二分查找  有序数组
func BinaryFind (arr *[10]int, leftIndex, rightIndex, findVal int) {
	mid := (leftIndex + rightIndex) / 2
	if leftIndex > rightIndex {
		fmt.Println("没有找到这个数")
		return
	} 
	if findVal < arr[mid] {
		BinaryFind(arr, leftIndex, mid - 1, findVal)
	} else if  findVal > arr[mid] {
		BinaryFind(arr, mid + 1, rightIndex, findVal)	
	} else {
		fmt.Println("找到了，下标为", mid)
	}
	
}
func main() {

	arr := [10]int{0, 4, 5, 8, 9, 12, 34, 76, 98, 435}
	BinaryFind(&arr, 0, 9, 9)

}