package main
import (
	"fmt"
)

//顺序查找
func main() {

	srr := [10]int{8, 5, 9, 34, 4, 76, 12, 0, 435, 98}
	var n int
	fmt.Println("请输入想要查找的数字")
	fmt.Scanln(&n)

	index := -1
	for i := 0; i < 10; i++ {
		if n == srr[i] {
			index = i
			fmt.Printf("找到了%v,下标是%v",n, index)
			break
		}
	}
	if index == -1 {
		fmt.Printf("没有找到%v",n)
	}
}
