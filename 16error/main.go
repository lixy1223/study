package main

import (
	"fmt"
	"errors"
)

func test() {
	//defer + recover 用于捕获和处理异常
	defer func() {
		err := recover() //捕获异常的内置函数
		if err != nil { //捕获到错误
			fmt.Println("error:", err)
		}
	} ()
	n1, n2 := 10, 0
	res := n1 / n2
	fmt.Println("res=", res)
}

//自定义错误
//读取文件名，若不是config.ini则抛出“读取文件错误”
func readConfig(name string) (err error) {
	if name == "config.ini" {
		return nil
	} else {
		return errors.New("读取文件错误")
	}
}

func test02() {

	err := readConfig("config2.ini")
	if err != nil {
		panic(err) //输出错误并终止程序
	}
}

func main() {

	test()  //输出错误，继续执行程序
	fmt.Println("1")

	test02()  //输出错误，终止程序
	fmt.Println("2") 
}
