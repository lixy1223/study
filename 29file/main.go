package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	//打开文件

	file, err := os.Open("e:/test.txt")
	if err != nil {
		fmt.Println("open file err=", err)
	}

	fmt.Printf("file=%v\n", file) //指针

	//关闭文件
	defer file.Close()

	//带缓存地读取文件
	reader := bufio.NewReader(file)
	//循环读取文件内容
	for {
		str, err := reader.ReadString('\n') //读到换行结束
		if err == io.EOF {                  //到文件末尾
			break
		}
		fmt.Print(str)
	}

	content, err := ioutil.ReadFile("e:/test.txt") //不带缓存一次性读取，包含打开和关闭
	if err != nil {
		fmt.Printf("read file err=%v", err)
	}
	fmt.Printf("%s\n", content)

	//创建一个新文件，写入5句 "hello, world"
	//指定路径
	filePath := "e:/b.txt"
	newFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Printf("open newFile err=%v\n", err)
		return
	}
	//及时关闭file句柄
	defer newFile.Close()

	str := "hello,world\n"
	//写入时，使用带缓存的 *Writer
	writer := bufio.NewWriter(newFile)
	for i := 0; i < 5; i++ {
		writer.WriteString(str)
	}
	//因为writer是带缓存，因此在调用WriterString方法时，其实
	//内容是先写入到缓存的,所以需要调用Flush方法，将缓冲的数据
	//真正写入到文件中， 否则文件中会没有数据!!!
	writer.Flush()

	//将一个文件中的内容写入另一个文件中(会清除写入文件中原有的内容，若没有该文件，则创建并写入)
	err = ioutil.WriteFile("e:/c.txt", content, 0666)
	if err != nil {
		fmt.Printf("write file error=%v\n", err)
	}
}
